import { Timestamp } from "rxjs";

export interface Post {
  id: number,
  title: string,
  content: string,
  author: string,
  creation_date: string
}