import { Post } from './../post';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PostService {
  url: string = 'https://node-rest-api-blog.herokuapp.com/posts';
  headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Authorization', 'Basic dXNlcjpwYXNzd29yZA==');

  constructor(private http: HttpClient) {}

  async getPosts(
    sort_by?: string,
    order_by?,
    limit?: number,
    offset?: number
  ): Promise<any> {
    const results = await this.http
      .get(
        `${this.url}/?sort_by=${sort_by || 'id'}&order_by=${
          order_by || 'asc'
        }&limit=${limit || 10}&offset=${offset || 0}`,
        {
          headers: this.headers,
        }
      )
      .toPromise();
    return results;
  }

  async createPost(post): Promise<any> {
    const results = await this.http
      .post(this.url, post, { headers: this.headers })
      .toPromise();
    return results;
  }

  async updatePost(post): Promise<any> {
    const { id } = post;
    const results = await this.http
      .put<Post>(`${this.url}/${id}`, post, {
        headers: this.headers,
      })
      .toPromise();
    return results;
  }

  async deletePost(id): Promise<any> {
    const results = await this.http
      .delete(`${this.url}/${id}`, { headers: this.headers })
      .toPromise();
    return results;
  }
}
