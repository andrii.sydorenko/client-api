import { ModalFormComponent } from './modal-form/modal-form.component';
import { TableComponent } from './table.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeagoClock, TimeagoModule } from 'ngx-timeago';
import { MyClock } from '../MyClock';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableRoutingModule } from './table-routing.module';

@NgModule({
  declarations: [TableComponent, ModalFormComponent],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    TimeagoModule.forRoot({
      clock: { provide: TimeagoClock, useClass: MyClock },
    }),
    TableRoutingModule,
  ],
  exports: [TableComponent, ModalFormComponent],
})
export class TableModule {}
