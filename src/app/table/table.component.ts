import { PostService } from '../services/post.service';
import { Post } from './../post';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { title } from 'process';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class TableComponent implements OnInit {
  constructor(
    private postService: PostService,
    private modalService: NgbModal,
    config: NgbModalConfig
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    this.getPosts();
  }

  posts: Post[] = [];
  sortedBy: string = 'id';
  filterBy: string = '';
  page: number = 1;
  posts_count: number;
  limit: number = 10;
  isActive = {
    id: 'before',
    title: 'disabled',
    content: 'disabled',
    author: 'disabled',
    creation_date: 'disabled',
  };

  open(content): void {
    this.modalService.open(content);
  }

  onPageChanged = async () => {
    let order_by = this.isActive[this.sortedBy] == 'after' ? 'desc' : 'asc';
    if (this.sortedBy == 'creation_date') {
      order_by = order_by == 'asc' ? 'desc' : 'asc';
    }
    await this.getSortedPosts();
  };

  onActive(param): void {
    const keys = Object.keys(this.isActive);

    for (let key of keys) {
      if (key != param) {
        this.isActive[key] = 'disabled';
      }
    }

    if (this.isActive[param] == 'disabled') {
      this.isActive[param] = 'before';
    } else if (this.isActive[param] == 'before') {
      this.isActive[param] = 'after';
    } else {
      this.isActive[param] = 'before';
    }

    this.onSort(param);
  }

  getPosts(
    sort_by?: string,
    order_by?: string,
    limit?: number,
    offset?: number
  ) {
    this.postService.getPosts(sort_by, order_by, limit, offset).then((data) => {
      this.posts_count = data['posts_count'];
      this.posts = data['posts'];
    });
  }

  async onAdd() {
    const post = {
      ...this.createPostForm.value,
    };

    await this.postService.createPost({ ...post });

    this.getSortedPosts();

    this.posts_count++;

    this.createPostForm.reset();
  }

  async onEdit(index) {
    const { title, content, author } = this.updatePostForm.value;
    const post = this.posts[index];

    const newPost = {
      ...post,
      title: title || post.title,
      content: content || post.content,
      author: author || post.author,
    };

    await this.postService.updatePost(newPost).then();

    this.getSortedPosts();

    this.updatePostForm.reset();
  }

  async onDelete(index) {
    const postId = this.posts[index].id;
    await this.postService.deletePost(postId).then();
    this.posts.splice(index, 1);
    this.posts_count--;

    if (this.page > 1 && this.posts.length === 0) {
      this.page--;
      this.onPageChanged();
    }

    if (this.page == 1 && this.posts.length === 0) {
      this.getSortedPosts();
    }
  }

  onSort(param: string): void {
    this.sortedBy = param;

    this.getSortedPosts();
  }

  getSortedPosts() {
    const param = this.sortedBy;
    if (this.isActive[param] == 'before') {
      const order_by = param == 'creation_date' ? 'desc' : 'asc';
      this.getPosts(param, order_by, this.limit, (this.page - 1) * this.limit);
    } else if (this.isActive[param] == 'after') {
      const order_by = param == 'creation_date' ? 'asc' : 'desc';
      this.getPosts(param, order_by, this.limit, (this.page - 1) * this.limit);
    }
  }

  setForm(index) {
    const { title, content, author } = this.posts[index];
    this.updatePostForm.get('title').setValue(title);
    this.updatePostForm.get('content').setValue(content);
    this.updatePostForm.get('author').setValue(author);
  }

  filterPosts() {
    console.log(this.filterBy);
  }

  createPostForm: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.maxLength(30),
      Validators.pattern(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/),
      this.noWhitespaceValidator,
    ]),
    content: new FormControl('', [
      Validators.required,
      Validators.maxLength(500),
      this.noWhitespaceValidator,
    ]),
    author: new FormControl('', [
      Validators.required,
      Validators.maxLength(30),
      Validators.pattern(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/),
      this.noWhitespaceValidator,
    ]),
  });

  updatePostForm: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.maxLength(30),
      Validators.pattern(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/),
      this.noWhitespaceValidator,
    ]),
    content: new FormControl('', [
      Validators.required,
      Validators.maxLength(500),
      this.noWhitespaceValidator,
    ]),
    author: new FormControl('', [
      Validators.required,
      Validators.maxLength(30),
      Validators.pattern(/^[^!"#$%&'()*+,-./:;<=>?@[\]^`{|}~]*$/),
      this.noWhitespaceValidator,
    ]),
  });

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = control.value
      ? (control.value || '').trim().length === 0
      : false;
    const isValid = !isWhitespace;
    return isValid ? null : { whitespace: true };
  }
}
