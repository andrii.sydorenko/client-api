import { FormGroup } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal-form',
  templateUrl: './modal-form.component.html',
  styleUrls: ['./modal-form.component.scss'],
})
export class ModalFormComponent implements OnInit {
  @Input() action: string;
  @Input() c;
  @Input() d;
  @Input() form: FormGroup;

  @Output() postSubmited: EventEmitter<string> = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  onSubmit(): void {
    this.postSubmited.emit('submited');
  }

  public clearForm() {
    this.form.reset();
  }
}
